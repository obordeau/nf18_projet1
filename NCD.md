Le Musée du Louvre est le plus grand musée de France et compte parmi 480 000 œuvres, de nombreuses expositions permettent de presenter ces oeuvres aux grand public. Certaines sont permanentes comme par exemple l'exposition "l'art du portrait" mais d'autres sont temporaires. Nous avons donc besoin de créer une base de données pour gérer ces différents événements. Nous cherchons a modéliser une base donnée claire, utile et agréable à manipuler.

# _Objets_ </br>
_Voici une liste des objets et de leurs attribut que nous devons implémenter :_

__OBJET : oeuvres__
```
- titre : texte
- dimensions : texte
- type : énumération parmis peinture, sculpture, dessin
- date de création : date
- artiste : texte (relié a la classe "artiste")
```

Les dimensions et la date de création peuvent être nulles si on ne les connait pas. L'artiste aussi peut-être nul si il est inconnu. SI le type ne fait pas partie de la liste prédéfinie (peinture, sculpture, dessin) alors on le laissera nul.

On choisira de représenter les oeuvres extérieures et les oeuvres du louvre avec un héritage de cette classe détaillé dans le document à la partie "Héritages".

__OBJET : artiste__
```
- nom et prenom : texte
- dates
```

Le nom et le prénom ne peuvent pas être nuls. 

Elles peuvent être acquises ou empruntées ce pourquoi nous allons utiliser un heritage. ce qui permettra de préciser le prix d'acquisition pour celles appartenant au Musée du Louvre.



__OBJET : Musée_exterieur__
```
- nom : chaine
- adresse, code postal, ville : texte 
```

Aucun élément de la table ne peut être nul pour une meilleure communication avec les musées extérieurs lors des emprunts et prêts d'oeuvres.

__OBJET : Expositions__
```
- nom : texte (UNIQUE)
```

Les expositions permanentes et temporaires aussi seront modelisées par un heritage et ce afin de conserver les informations propres a chacune. Une exposition temporaire sera aussi précisée avec ses dates. 


__OBJET : Salles__
```
- numero : entier (UNIQUE)
- capacité : entier 
```
Une salle est composée de panneaux explicatifs, nous pouvons donc utiliser une __composition__. En effet un panneaux dépends entierement de son affectation à la salle et n'existe pas si cette salle n'est plus utilisée.
</br>
> composition 
> __OBJET : Panneauxexplicatifs__


__OBJET : Prestataire__
```
- raison_sociale: chaine de caractère (UNIQUE)
```

Les rénovations des oeuvres sera modélisée par une classe d'association entre oeuvre et prestataire


Gestion des guides :
  
__OBJET Guides__
```
- identifiant :  chaine de caractère (UNIQUE)
- nom et prénom  :  chaines de caractères
- adresse : texte
- date embauche : date
```

Les affectations des guides aux expositions permanentes seront modélisée par une classe d'association entre les deux classes avec des créneaux choisis dans une énumération de créneaux prédéfinis.

(résumé)
- liste des propriétés de chaque objet
	- Oeuvre (titre, dimensions, type (peinture | sculture | photo), date) 
		<-- Oeuvre acquise (prix, date d'acquisition)
        <-- Oeuvre extérieure (musée d'origine)
	- Artiste (nom, prénom, naissance, mort)
    - Exposition (nom)
        <-- Exposition temporaire (début, fin)
        <-- Exposition permanente
    - Musée exterieur (nom, adresse, code postale, ville)
    - Guide (nom, prenom, adresse, date d'embauche)
    - Restauration (type, date, montant)
    - Prestataire (raison sociale)
    - Salle (num, capacité)
    - Panneau (description)


 # _Classes d'Associations Importantes_ :
</br>

Ils est aussi important de préciser certaines classes d'associations, comme les renovations qui associent oeuvres et prestataires
</br>

__Renovations__

```
- type : texte
- date : date 
- horaire : entier 
- montant : entier ou décimal 
``` 
__Prête__
```
- début : date
- fin : date
```

__Emprunte__
```
- début : date
- fin : date
```

__Est affecté__
```
- jour : énumération (lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche)
- créneau : énumération (9h-11h, 11h-13h, 13h-15h, 15h-17h)
```

 # _Héritages_ : 

Les oeuvres empruntées et pretées hérite de tous les attributs de l'objets oeuvre (caractéristiques permanente), pour les oeuvres possedée par le louvre on veut garder en memoir le prix d'acqusition et pour celmles appartenants a un musée exterieur on précisera de quel musée il provient par une association
Nous avons décider de modideliser cet heriatge par deux classe filles completées d'une vue dynamique qui represente la classe oeuvre
.

__OBJET HERITE oeuvre_louvre__
```
- prix acquisition : long int 
- date acquisition : date
```

Ces deux attributs sont non nuls pour une meilleure traçabilité le musée est obligé de les connaitre.

__OBJET HERITE oeuvre_exterieur__

Les expositions sont de deux types soit permanentes soit temporaires. Pour chacunes on veux stocker des informations qui sont propres à leur type, on va donc utiliser un héritage. 

</br>

__OBJET HERITE expo_permanentes__

__OBJET HERITE expo_temporaires__
```
- date de début : date
- date de fin : date
``` 

Tous les héritages sont __exclusifs__, de plus on considère que le nom d'une exposition temporaire ne peut être réutilisé.
</br>

# _Contraintes_ : 

- sur tous les couples d'attribut de type datedebut/date fin : datedebut \< datefin

 # _Hypothèses effectuées_ : 
 - On considère que les expositions temporaires sont exclusivement composées d’œuvres issues de musées extérieurs (provenant de prêts par les musées extérieurs).
 - On ne considère seulement les salles disponibles pour les expositions temporaires (celles prévues pour les expositions permanentes ne variant pas, on ne les considére pas dans la base de données). 
 - On considère que les guides ont un planning fixe pour les expositions permanentes (il ne varie pas chaque semaine). De manière exceptionnelle, les guides pourront également participer aux expositions temporaires en plus de leurs planning régulier. Nous ne gérerons pas de planning précis pour leur affectation aux expositions temporaires car celles-ci s'ajouteront donc dans leurs créneaux libres.
 - Nous avons choisi d'instancier tous les prestataires qui travaillent avec le Louvre.
 - Les prestataires du Louvre ne restaurent que les œuvres du Louvre.
 - Les œuvres du Louvre ne peuvent pas être prêtées plusieurs fois au même musée.
 - Le Louvre ne peut pas emprunter plusieurs fois la même œuvre à un musée.
 - Toutes les expositions permanentes ont des noms différents.
 - Toutes les expositions temporaires, même si elles sont finies, ont des noms différents.


# _Accès_ : 

</br>Il es utile de définir différents profils qui n'ont pas les même accès dans la base de donnée :

1. Les administrateurs, ceux qui organisent et gère le Louvre 
- modifier et écrire dans la base de donnée. 
- exécuter des requêtes 
2. guides, ceux qui présentent les oeuvres
- accès de lecture  pour consulter leurs affectations aux expos, consulter les informations sur les oeuvres. 
3. Prestataires, ceux qui rénovent les oeuvres 
- lecture pour consulter les horaires d'une restaurations
4. visiteur 
- droit de lecture consulter les information de lieu et de temps des différentes expositions 

2/3/4/ n'accède dans la vraie vie qu'a une interface de présentation des données stockés 
 
         
