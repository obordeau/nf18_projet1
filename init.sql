DO $$ 
  DECLARE 
    r RECORD;
BEGIN
  FOR r IN 
    (
      SELECT table_name 
      FROM information_schema.tables 
      WHERE table_schema=current_schema()
    ) 
  LOOP
     EXECUTE 'DROP TABLE IF EXISTS ' || quote_ident(r.table_name) || ' CASCADE';
  END LOOP;
END $$ ;

CREATE TABLE Artiste(
    IDA SERIAL PRIMARY KEY,
    nom varchar NOT NULL,
    prenom varchar,
    naiss DATE,
    mort DATE
);

CREATE TYPE TYPE_OEUVRE AS ENUM ('peinture', 'sculpture', 'photographie');

CREATE TABLE Musee_ext(
	IDM SERIAL PRIMARY KEY,
	nom VARCHAR NOT NULL,
	adresse TEXT NOT NULL,
	CP VARCHAR NOT NULL,
	ville VARCHAR NOT NULL
);

CREATE TABLE Expo_temp(
	nom VARCHAR PRIMARY KEY,
	debut DATE NOT NULL,
	fin DATE NOT NULL
);

CREATE TABLE Expo_perm(
	nom VARCHAR PRIMARY KEY
);

CREATE TABLE Oeuvre_ext(
	IDO SERIAL PRIMARY KEY,
    titre VARCHAR NOT NULL,
    dimensions VARCHAR,
    type TYPE_OEUVRE,
    date_crea INTEGER,
    auteur INTEGER,
	expo VARCHAR,
	FOREIGN KEY (auteur) REFERENCES Artiste(IDA),
    FOREIGN KEY (expo) REFERENCES Expo_temp(nom)
);

CREATE TABLE Oeuvre_louvre(
	IDO SERIAL PRIMARY KEY,
    titre VARCHAR NOT NULL,
    dimensions VARCHAR,
    type TYPE_OEUVRE,
    date_crea INTEGER,
    auteur INTEGER,
    prix_acq INTEGER NOT NULL,
    date_acq DATE NOT NULL,
	expo VARCHAR,
    FOREIGN KEY (auteur) REFERENCES Artiste(IDA),
	FOREIGN KEY (expo) References Expo_perm(nom)
);

CREATE TABLE Prete(
	IDO integer,
	musee integer,
	debut DATE NOT NULL,
	fin DATE NOT NULL,
	FOREIGN KEY (IDO) REFERENCES Oeuvre_ext(IDO),
	FOREIGN KEY (musee) REFERENCES Musee_ext(IDM),
	PRIMARY KEY (IDO, musee)
);

CREATE TABLE Emprunte(
	IDO INTEGER,
	musee INTEGER,
	debut DATE NOT NULL,
	fin DATE NOT NULL,
	FOREIGN KEY (IDO) REFERENCES Oeuvre_louvre(IDO),
	FOREIGN KEY (musee) REFERENCES Musee_ext(IDM),
	PRIMARY KEY (IDO, musee)
);

CREATE TABLE Prestataire(
    IDP SERIAL PRIMARY KEY,
    raison_sociale VARCHAR NOT NULL
);

CREATE TABLE Restaurer(
	prestataire INTEGER,
	oeuvre SERIAL,
	type VARCHAR NOT NULL,
	fin DATE NOT NULL,
	montant INTEGER NOT NULL,
	FOREIGN KEY (prestataire) REFERENCES Prestataire(IDP),
	FOREIGN KEY (oeuvre) REFERENCES Oeuvre_louvre(IDO),
	PRIMARY KEY (prestataire, oeuvre, fin)
);

CREATE TABLE Guide(
	IDG SERIAL PRIMARY KEY,
	nom VARCHAR NOT NULL,
	prenom VARCHAR NOT NULL,
	adresse TEXT NOT NULL,
	date_embauche DATE NOT NULL
);

CREATE TYPE JOUR AS ENUM ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');

CREATE TYPE CRENEAU AS ENUM ('9-11', '11-13', '13-15', '15-17');

CREATE TABLE Affecter_perm(
	expoPerm VARCHAR,
	guide INTEGER,
	jour JOUR,
	creneau CRENEAU,
	FOREIGN KEY (expoPerm) REFERENCES Expo_perm(nom),
	FOREIGN KEY (guide) REFERENCES Guide(IDG),
	PRIMARY KEY (expoPerm, guide, jour, creneau)
);

CREATE TABLE Affecter_temp(
	expoTemp VARCHAR,
	guide INTEGER,
	FOREIGN KEY (expoTemp) REFERENCES Expo_temp(nom),
	FOREIGN KEY (guide) REFERENCES Guide(IDG),
	PRIMARY KEY (expoTemp, guide)
);

CREATE TABLE Salle(
	num INTEGER PRIMARY KEY,
	capacite INTEGER NOT NULL
);

CREATE TABLE Exposer(
    salle INTEGER,
	expo VARCHAR,
	FOREIGN KEY (salle) REFERENCES Salle(num),
	FOREIGN KEY (expo) REFERENCES Expo_temp(nom),
	PRIMARY KEY (salle, expo)
); 

CREATE TABLE Panneau(
	ID SERIAL,
	salle INTEGER,
	description TEXT,
	FOREIGN KEY (salle) REFERENCES Salle(num),
	PRIMARY KEY (ID, salle)
);

CREATE view vOeuvres as
SELECT oe.ido, oe.titre, oe.dimensions, oe.type, oe.date_crea, oe.auteur, NULL as prix, NULL as dateacq FROM oeuvre_ext oe 
UNION ALL 
SELECT ol.ido, ol.titre, ol.dimensions, ol.type, ol.date_crea, ol.auteur, ol.prix_acq as prix, ol.date_acq as dates FROM oeuvre_louvre ol;
CREATE VIEW vExpos AS 
SELECT * FROM Expo_Perm
UNION ALL 
SELECT nom FROM Expo_Temp ;


INSERT INTO Artiste (nom, prenom, naiss, mort) VALUES ('Rodin', 'Auguste', TO_DATE('1840-11-12', 'YYYY-MM-DD'), TO_DATE('1917-11-17', 'YYYY-MM-DD'));
INSERT INTO Artiste (nom, prenom, naiss, mort) VALUES ('De Vinci', 'Léonard', TO_DATE('1452-04-14', 'YYYY-MM-DD'), TO_DATE('1519-05-02', 'YYYY-MM-DD'));
INSERT INTO Artiste (nom, prenom, naiss, mort) VALUES ('Monet', 'Claude', TO_DATE('1840-11-14', 'YYYY-MM-DD'), TO_DATE('1926-12-05', 'YYYY-MM-DD'));
INSERT INTO Artiste (nom, prenom, naiss, mort) VALUES ('Picasso', 'Pablo', TO_DATE('1881-10-25', 'YYYY-MM-DD'), TO_DATE('1973-04-08', 'YYYY-MM-DD'));
INSERT INTO Artiste (nom, prenom, naiss, mort) VALUES ('Canova', 'Antonio', TO_DATE('881-10-25', 'YYYY-MM-DD'), TO_DATE('973-04-08', 'YYYY-MM-DD'));

INSERT INTO Expo_temp VALUES ('Impressionniste', TO_DATE('2021-06-01', 'YYYY-MM-DD'), TO_DATE('2021-12-01', 'YYYY-MM-DD'));
INSERT INTO Expo_temp VALUES ('Cubisme', TO_DATE('2021-12-01', 'YYYY-MM-DD'), TO_DATE('2022-12-01', 'YYYY-MM-DD'));
INSERT INTO Expo_temp VALUES ('Surréalisme', TO_DATE('2021-01-01', 'YYYY-MM-DD'), TO_DATE('2024-01-01', 'YYYY-MM-DD'));

INSERT INTO Expo_perm VALUES ('Femmes');
INSERT INTO Expo_perm VALUES ('Grece antique');

INSERT INTO Musee_ext (nom, adresse, CP, ville) VALUES ('Musée Rodin', '77 rue de Varenne', '75007', 'Paris');
INSERT INTO Musee_ext (nom, adresse, CP, ville) VALUES ('Museo nacional centro de arte Reina Sofia', 'Santa Isabel 52', '28012', 'Madrid');
INSERT INTO Musee_ext (nom, adresse, CP, ville) VALUES ('Musée de l''Orangerie', 'Jardin des Tuileries', '75001', 'Paris');
INSERT INTO Musee_ext (nom, adresse, CP, ville) VALUES ('Musée d''Orsay', '62 rue de Lille', '75007', 'Paris');
INSERT INTO Musee_ext (nom, adresse, CP, ville) VALUES ('Centre Georges Pompidou', 'place Georges-Pompidou', '75004', 'Paris');

INSERT INTO Oeuvre_ext (titre, dimensions, type, date_crea, auteur, expo) VALUES ('Le penseur', '1.8 x 0.98 x 1.45', 'sculpture', 1880, 1, 'Impressionniste');
INSERT INTO Oeuvre_ext (titre, dimensions, type, date_crea, auteur, expo) VALUES ('Les nymphéas', '0.219 x 0.602', 'peinture', 1920, 3, 'Impressionniste');
INSERT INTO Oeuvre_ext (titre, dimensions, type, date_crea, auteur, expo) VALUES ('Guernica', '0.349 x 0.776', 'peinture', 1937, 4, 'Impressionniste');
INSERT INTO Oeuvre_ext (titre, dimensions, type, date_crea, auteur, expo) VALUES ('Trois femmes', '200 × 178 cm', 'peinture', 1908, 4, 'Cubisme');
INSERT INTO Oeuvre_ext (titre, dimensions, type, date_crea, auteur, expo) VALUES ('Guitare', '111 × 63 x 26 cm', 'sculpture', 1924, 4, 'Surréalisme');

INSERT INTO Oeuvre_louvre (titre, dimensions, type, date_crea, auteur, prix_acq, date_acq, expo) VALUES ('La joconde', '0.77 x 0.53', 'peinture', 1510, 2, 10000000, TO_DATE('1990-01-01', 'YYYY-MM-DD'), 'Femmes');
INSERT INTO Oeuvre_louvre (titre, dimensions, type, date_crea, auteur, prix_acq, date_acq) VALUES ('Annonciation', '0.98 x 0.217', 'peinture', 1480, 2, 10000, TO_DATE('2001-04-01', 'YYYY-MM-DD'));
INSERT INTO Oeuvre_louvre (titre, dimensions, type, date_crea, auteur, prix_acq, date_acq, expo) VALUES ('Autopotrait', '0.81 x 0.60', 'peinture', 1901, 4, 20000, TO_DATE('2006-07-01', 'YYYY-MM-DD'), 'Cubisme');
INSERT INTO Oeuvre_louvre (titre, dimensions, type, date_crea, auteur, prix_acq, date_acq, expo) VALUES ('hydre', '1.00 x 0.53', 'sculpture', 810, 5, 1906500, TO_DATE('1890-01-01', 'YYYY-MM-DD'), 'Grece antique');

INSERT INTO Prete VALUES (1, 1, TO_DATE('2020-01-01', 'YYYY-MM-DD'), TO_DATE('2022-01-01', 'YYYY-MM-DD'));
INSERT INTO Prete VALUES (2, 3, TO_DATE('2021-05-01', 'YYYY-MM-DD'), TO_DATE('2024-06-01', 'YYYY-MM-DD'));
INSERT INTO Prete VALUES (3, 2, TO_DATE('2018-01-01', 'YYYY-MM-DD'), TO_DATE('2022-07-01', 'YYYY-MM-DD'));
INSERT INTO Prete VALUES (4, 2, TO_DATE('2021-10-01', 'YYYY-MM-DD'), TO_DATE('2023-01-01', 'YYYY-MM-DD'));
INSERT INTO Prete VALUES (5, 2, TO_DATE('2020-12-01', 'YYYY-MM-DD'), TO_DATE('2024-02-01', 'YYYY-MM-DD'));

INSERT INTO Emprunte VALUES (2, 4, TO_DATE('2020-08-01', 'YYYY-MM-DD'), TO_DATE('2023-01-01', 'YYYY-MM-DD'));

INSERT INTO Prestataire (raison_sociale) VALUES ('PME');

INSERT INTO Restaurer VALUES (1, 2, 'Dorures', TO_DATE('2021-11-10', 'YYYY-MM-DD'), 15000);
INSERT INTO Restaurer VALUES (1, 3, 'Traitement de lumière', TO_DATE('2022-05-06', 'YYYY-MM-DD'), 15000);

INSERT INTO Guide (nom, prenom, adresse, date_embauche) VALUES ('Schneider', 'Martin', '8 BLVD VICTOR HUGO', TO_DATE('2020-05-01', 'YYYY-MM-DD'));
INSERT INTO Guide (nom, prenom, adresse, date_embauche) VALUES ('Sellier', 'Barnabé', '15 RUE DES LOMBARDS', TO_DATE('2021-07-01', 'YYYY-MM-DD'));

INSERT INTO Salle VALUES (1, 150);
INSERT INTO Salle VALUES (10, 100);
INSERT INTO Salle VALUES (5, 350);

INSERT INTO Panneau (salle, description) VALUES (1, 'Penseur de Rodin');
INSERT INTO Panneau (salle, description) VALUES (1, 'Nymphéas');
INSERT INTO Panneau (salle, description) VALUES (1, 'Guernica');

INSERT INTO Exposer VALUES (1, 'Impressionniste');
INSERT INTO Exposer VALUES (1, 'Cubisme');
INSERT INTO Exposer VALUES (10, 'Surréalisme');

INSERT INTO Affecter_temp VALUES ('Impressionniste', 1);

INSERT INTO Affecter_perm VALUES ('Femmes', 1, 'Lundi', '9-11');
INSERT INTO Affecter_perm VALUES ('Femmes', 1, 'Lundi', '11-13');
INSERT INTO Affecter_perm VALUES ('Femmes', 1, 'Lundi', '13-15');
INSERT INTO Affecter_perm VALUES ('Femmes', 2, 'Mardi', '9-11');
INSERT INTO Affecter_perm VALUES ('Femmes', 2, 'Mardi', '13-15');
