Dans expo_perm.json et expo_temp.json, nous avons utilisé une représentation avec imbrication redondante pour les guides, les salles et les panneaux, c'est à dire qu'ils sont décrits plusieurs fois dans une exposition.
Et pour les oeuvres nous avons utilisé une liste avec leur ids, les oeuvre seraient implémentées dans un autre fichier json.

Pour écrire les tables des oeuvres en JSON, on doit adopter une des deux méthodes pour gérer les clés étrangère:<br>
    -soit on met juste l'id correspondant <br>
    -soit on redéfinit à chaque fois (imbrication redondante)
    
Si par exemple on choisit la première méthode, l'attribut auteur serait un entier et expo une liste d'entier.

L'inconvénient du NoSQL est la redondance et la possiblité de non-cohérence des données, mais il a l'avantage d'être plus souple et plus rapide qu'une base de données relationnelle.
