import psycopg2
import datetime

conn = psycopg2.connect("host=%s dbname=%s user=%s password=%s" % (
    "tuxa.sme.utc", "dbnf18a037", "nf18a037", "1AfyGsAH"))


def insert_date():
    andeb = -1
    while andeb not in range(1000, 2050):
        andeb = int(input("Entrez l'année de début\n"))
    mdeb = 0
    while mdeb not in range(1, 13):
        mdeb = int(input("Entrez le mois de début\n"))
    jdeb = 0
    while jdeb not in range(1, 32):
        jdeb = int(input("Entrez le jour de début\n"))
    anfin = -1
    while anfin not in range(1000, 2050):
        anfin = int(input("Entrez l'année de fin\n"))
    mfin = 0
    while mfin not in range(1, 13):
        mfin = int(input("Entrez le mois de fin\n"))
    jfin = 0
    while jfin not in range(1, 32):
        jfin = int(input("Entrez le jour de fin\n"))
    date1 = datetime.date(andeb, mdeb, jdeb)
    date2 = datetime.date(anfin, mfin, jfin)
    if date1 > date2:
        insert_date()
    else:
        return [f"'{andeb}-{mdeb}-{jdeb}'", f"'{anfin}-{mfin}-{jfin}'"]


def insert_musee():
    nom = input("\nEntrez le nom du musée\n")
    codep = input("\ncode postal du musée : \n")
    ville = input("\nville du musee\n")
    adresse = input("\nadresse complete du musée : \n")
    cur = conn.cursor()
    if nom and codep and ville and adresse:
        try:
            sql = f"INSERT INTO MUSEE_EXT ( nom, adresse, cp, ville) VALUES ('{nom}', '{adresse}', '{codep}', '{ville}');"
            cur.execute(sql)
            conn.commit()
        except psycopg2.DataError as e:
            print("echec de l'insertion")
            print(e)
    else:
        print("tentative d'ajout d'une valeur nulle, retour")
        print()


def insert_oeuvreext():
    cur = conn.cursor()
    nom = input("\nEntrez le nom de l'oeuvre extérieure à ajouter\n")
    type = input(
        "\nEntrez le type de l'oeuvre à ajouter (peinture, sculpture, photographie)\n")
    date = int(input("\nEntrez la date de création de l'oeuvre\n"))
    try:
        sql = f"INSERT INTO OEUVRE_EXT (titre, type, date_crea) VALUES ('{nom}', '{type}', {date}) RETURNING IDO;"
        cur.execute(sql)
        ido = cur.fetchone()[0]
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("echec de l'insertion")
        print(e)
        return
    sql = "SELECT idm, nom FROM Musee_ext"
    cur.execute(sql)
    all = cur.fetchall()
    print("\nLes musées extérieurs sont:")
    i = 1
    for raw in all:
        print(f"  Musée {i} : {raw[1]}")
        i += 1
    res = int(input(
        "\nA quel musée extérieur voulez-vous ajouter l'oeuvre ? (Entrez le numéro du musée)\n"))
    print("\nPour le pret de l'oeuvre :\n")
    date = insert_date()
    try:
        sql = f"INSERT INTO PRETE VALUES ({ido}, {all[res-1][0]}, TO_DATE({date[0]}, 'YYYY-MM-DD'), TO_DATE({date[1]}, 'YYYY-MM-DD'));"
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\nContrainte non respectée")
        print("Message système :", e)
        return
    except psycopg2.IntegrityError as e:
        conn.rollback()
        print("\nContrainte non respectée")
        print("Message système :", e)
        return


def insert_expoperm():
    nom = input("\nEntrez le nom de l'exposition\n")
    cur = conn.cursor()
    if nom:
        try:
            sql = f"INSERT INTO EXPO_PERM VALUES ('{nom}');"
            cur.execute(sql)
            conn.commit()
        except psycopg2.DataError as e:
            print("echec de l'insertion")
            print(e)
    else:
        print("tentative d'ajout d'une valeur nulle, retour")
        print()


def liste_guide():
    cur = conn.cursor()
    sql = "SELECT IDG, nom, prenom, date_embauche FROM guide"
    cur.execute(sql)
    raw = cur.fetchone()
    print("\nListe des guides :")
    while raw:
        print(f"Guide {raw[0]} :")
        print(f"	Nom : {raw[1]}")
        print(f"	Prenom : {raw[2]}")
        print(f"	Date d'embauche : {raw[3]}")
        print()
        raw = cur.fetchone()


def liste_expos_permanentes():
    cur = conn.cursor()
    sql = "SELECT nom FROM Expo_perm"
    cur.execute(sql)
    raw = cur.fetchone()
    print("\nListe des expositions permanentes")
    while raw:
        print(f"Nom de l'exposition : {raw[0]}")
        raw = cur.fetchone()
    print()


def liste_expos_temporaires():
    cur = conn.cursor()
    sql = "SELECT nom, debut, fin, salle FROM Expo_temp ET LEFT OUTER JOIN Exposer E ON  ET.nom = E.expo WHERE fin > CURRENT_DATE;"
    cur.execute(sql)
    raw = cur.fetchone()
    print("\nListe des expositions temporaires")
    while raw:
        print()
        print(f"Nom de l'exposition: {raw[0]}")
        print(f"	Debut : {raw[1]}")
        print(f"	Fin : {raw[2]}")
        print(f"	Salle affectée : {raw[3]}")
        raw = cur.fetchone()
    print()


def liste_oeuvres_louvre():
    cur = conn.cursor()
    sql = "SELECT ido, titre, date_crea, type FROM Oeuvre_louvre OL ORDER BY type, date_crea ASC, titre ASC;"
    cur.execute(sql)
    raw = cur.fetchone()
    print("\nListe des oeuvre du Louvres :")
    while raw:
        print(f"Oeuvre numero : {raw[0]} :")
        print(f"	Titre : {raw[1]}")
        print(f"	Date de création : {raw[2]}")
        print(f"	Type : {raw[3]}")
        print()
        raw = cur.fetchone()


def info_expo():
    cur = conn.cursor()
    tmp = 0
    sql = "SELECT nom from expo_perm;"
    cur.execute(sql)
    raw = cur.fetchone()
    print("\nLes exposition permanentes sont:")
    while raw:
        print(f"{raw[0]}")
        raw = cur.fetchone()
    print()
    sql = "SELECT nom from expo_temp;"
    cur.execute(sql)
    raw = cur.fetchone()
    print("Les exposition temporaires sont:")
    while raw:
        print(f"{raw[0]}")
        raw = cur.fetchone()
    print()
    nom = input(
        "Vous voulez connaitre des informations sur quelle exposition ?\n")
    sql = f"SELECT expo, Count(*), Avg(prix_acq) FROM OEUVRE_LOUVRE GROUP BY expo HAVING expo='{nom}'"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print(f"\nExposition : {raw[0]}")
        print(f"	Nombre d'oeuvres : {raw[1]}")
        print(f"	Prix moyen d'acquisition : {raw[2]:.2f}")
        print()
        tmp = 1
    sql = f"SELECT expo, Count(*) FROM OEUVRE_EXT GROUP BY expo HAVING expo='{nom}'"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print(f"\nExposition {raw[0]}")
        print(f"	Nombre d'oeuvres : {raw[1]}")
        print()
        tmp = 1
    if tmp == 0:
        print("\nL'exposition n'existe pas\n")


def afficher_guide_expo():
    cur = conn.cursor()
    choixExpo = -1
    while(choixExpo != '1' and choixExpo != '2'):
        choixExpo = input(
            "\nExposition permanente (1) ou exposition temporaire (2)\n")

    if(choixExpo == '1'):
        sql = "SELECT nom from expo_perm;"
        cur.execute(sql)
        raw = cur.fetchone()
        print("\nLes exposition permanentes sont:")
        while raw:
            print(f"{raw[0]}")
            raw = cur.fetchone()
        expocol = "expoperm"
        affecter = "affecter_perm"
    else:
        sql = "SELECT nom from expo_temp;"
        cur.execute(sql)
        raw = cur.fetchone()
        print("\nLes exposition temporaires sont:")
        while raw:
            print(f"{raw[0]}")
            raw = cur.fetchone()
        expocol = "expotemp"
        affecter = "affecter_temp"

    expo = input("\nEntrez le nom de l'exposition:\n")
    sql = f"SELECT idg, nom, prenom FROM guide JOIN {affecter} as a ON a.guide=guide.idg WHERE a.{expocol} = '{expo}' GROUP BY nom, prenom, idg;"

    cur.execute(sql)
    result = cur.fetchall()
    print()
    for raw in result:
        print(f"Guide {raw[0]} : {raw[1]} {raw[2]}")
    print()


def liste_oeuvre_pretees():
    cur = conn.cursor()
    sql = "SELECT prete.IDO, titre, fin FROM prete JOIN oeuvre_louvre ON prete.IDO = oeuvre_louvre.ido WHERE debut < CURRENT_DATE AND fin > CURRENT_DATE ORDER BY fin;"
    cur.execute(sql)
    raw = cur.fetchone()
    print("Liste des oeuvre du Louvres en cours de prêt :")
    while raw:
        print(f"Oeuvre numero : {raw[0]} :")
        print(f"	Titre : {raw[1]}")
        print(f"	Date de rendu : {raw[2]}")
        print()
        raw = cur.fetchone()


def liste_oeuvres_absentes():
    cur = conn.cursor()
    sql = "SELECT nom, COUNT(oeuvre) FROM Expo_perm JOIN Oeuvre_louvre ON Expo_perm.nom = Oeuvre_louvre.expo JOIN Restaurer ON Oeuvre_louvre.IDO = Restaurer.oeuvre JOIN Prete ON Oeuvre_louvre.IDO = Prete.IDO WHERE (Restaurer.fin > CURRENT_DATE) OR (Prete.debut < CURRENT_DATE AND Prete.fin > CURRENT_DATE) GROUP BY nom;"
    cur.execute(sql)
    raw = cur.fetchone()
    print("\nListe des oeuvre absentes du Louvres actuellement :\n")
    while raw:
        print(
            f"Nom de l'exposition : {raw[0]}  Nombre d'oeuvres absentes : {raw[1]}")
        print()
        raw = cur.fetchone()


def liste_oeuvre_empruntees():
    cur = conn.cursor()
    sql = "SELECT Oeuvre_ext.IDO, expo, fin FROM Emprunte JOIN Oeuvre_ext ON Emprunte.IDO = Oeuvre_ext.IDO WHERE Emprunte.debut < CURRENT_DATE AND Emprunte.fin > CURRENT_DATE ORDER BY fin;"
    cur.execute(sql)
    raw = cur.fetchone()
    print("\nListe des oeuvres actuellement empruntées par le Louvre :\n")
    while raw:
        print(f"Oeuvre numero : {raw[0]} :")
        print(f"	Exposition asociée : {raw[1]}")
        print(f"	Date de rendu : {raw[2]}")
        print()
        raw = cur.fetchone()


def liste_oeuvre_periode():
    debut = int(input("\nEntrez l'année de début de la période\n>"))
    fin = int(input("\nEntrez l'année de fin de la période\n>"))
    cur = conn.cursor()
    sql = f"SELECT * FROM Oeuvre_louvre WHERE date_crea < {fin} and date_crea > {debut};"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print(f"Liste des oeuvres créées entre {debut} et {fin}:\n")
    while raw:
        print(f"Oeuvre numero : {raw[0]}")
        print(f"Nom : {raw[1]}")
        print(f"Dimension : {raw[2]}")
        print(f"Type : {raw[3]}")
        print(f"Date de création : {raw[4]}")
        print(f"Auteur : {raw[5]}")
        print(f"Prix d'acquisition : {raw[6]}")
        print(f"Date d'acquisition : {raw[7]}")
        print(f"Exposition asociée : {raw[8]}")
        print(f"Appartenance : Louvre")
        print(f"Durée : indéterminée")
        print()
        raw = cur.fetchone()

    sql = f"SELECT IDO, titre, dimensions, type, date_crea, auteur, expo, ROUND(fin - debut) FROM Oeuvre_ext JOIN Expo_temp ON Oeuvre_ext.expo = Expo_temp.nom WHERE date_crea < {fin} and date_crea > {debut};"
    cur.execute(sql)
    raw = cur.fetchone()
    while raw:
        print(f"Oeuvre numero : {raw[0]}")
        print(f"Nom : {raw[1]}")
        print(f"Dimension : {raw[2]}")
        print(f"Type : {raw[3]}")
        print(f"Date de création : {raw[4]}")
        print(f"Auteur : {raw[5]}")
        print(f"Exposition asociée : {raw[6]}")
        print(f"Appartenance : Autre")
        print(f"Durée : {raw[7]}")
        print()
        raw = cur.fetchone()


def temp_moyen_pret():
    cur = conn.cursor()
    sql = "SELECT idm, nom, adresse, AVG(ROUND(fin - debut)) FROM Prete JOIN Musee_ext ON prete.musee = musee_ext.IDM GROUP BY idm, musee_ext, adresse, nom;"
    cur.execute(sql)
    raw = cur.fetchone()
    print("Temps moyen d'un prêt (par musée):")
    while raw:
        print(f"musée partenaire : {raw[1]}")
        print(f"    adresse : {raw[2]}")
        print(f"	temps moyen des prêts : {raw[3]} jours")
        print()
        raw = cur.fetchone()


def informations_oeuvres():
    cur = conn.cursor()
    sql = 'SELECT  O.ido, O.titre , O.prix_acq, count(R.oeuvre) as "Nombre restaurations", sum(R.montant) as "Total montant restaurations" FROM OEUVRE_LOUVRE O \
    LEFT JOIN Restaurer R ON R.oeuvre = O.ido GROUP BY  O.ido, O.titre, O.prix_acq UNION ALL \
    SELECT oe.ido, oe.titre, NULL AS prix, -1 AS  "Nombre restaurations",  NULL AS   "Total montant restaurations" FROM oeuvre_ext oe;'
    cur.execute(sql)
    raw = cur.fetchone()
    print("Informations sur chaque oeuvre :")
    while raw:
        if raw[2] != None:
            print(f"Nom de l'oeuvre : {raw[1]}")
            print(f"    Prix d'acquisition : {raw[2]}")
            if raw[3] != 0:
                print(
                    f"    Prix total des {raw[3]} rennovation {raw[4]} euros")
            else:
                print("    Cette oeuvre n'a jamais été restaurée")
            print()
        else:
            print(f"Nom de l'oeuvre : {raw[1]}")
            print("    Empruntée d'un autre musée")
            print()
        raw = cur.fetchone()


def insert_salle():
    cur = conn.cursor()
    sql = "SELECT nom FROM Expo_temp;"
    cur.execute(sql)
    raws = cur.fetchall()
    print("\nVeuillez selectionner une des expositions temporaires :")
    for i, raw in enumerate(raws):
        print(f"{i} : {raw[0]}  ", end="")
    print("\n>")
    expo = raws[int(input())][0]
    non_init_rooms = list(range(1, 21))
    sql = "SELECT num from Salle"
    cur.execute(sql)
    raws = cur.fetchall()
    for i, raw in enumerate(raws):
        if raw[0] in non_init_rooms:
            del non_init_rooms[non_init_rooms.index(raw[0])]
    available_rooms = list(range(1, 21))
    sql = "SELECT salle from Exposer"
    cur.execute(sql)
    raws = cur.fetchall()
    for i, raw in enumerate(raws):
        if raw[0] in available_rooms:
            del available_rooms[available_rooms.index(raw[0])]
    if len(available_rooms) == 0:
        print("\nAucune salle disponible\n")
    else:
        print("\nSalles disponibles :")
        for room in available_rooms:
            print(f"{room}  ", end="")
        print(
            f"\nVeuillez choisir une salle à dédier à l'exposition {expo}\n>")
        salle = int(input())
        if salle in non_init_rooms:
            sql = f"INSERT INTO Salle VALUES ({salle}, 50);"
            try:
                cur.execute(sql)
                conn.commit()
            except psycopg2.IntegrityError as e:
                conn.rollback()
                print("contraintes non respectées\n")
                print(f"Message système : {e}\n")

        sql = f"INSERT INTO Exposer VALUES ({salle}, '{expo}');"
        try:
            cur.execute(sql)
            conn.commit()
        except psycopg2.IntegrityError as e:
            conn.rollback()
            print("contraintes non respectées\n")
            print(f"Message système : {e}\n")


def nb_creneaux():
    cur = conn.cursor()
    sql = 'SELECT IDG, nom, prenom, adresse, date_embauche, COUNT(Guide.IDG) FROM Guide JOIN Affecter_perm ON Guide.IDG = Affecter_perm.guide GROUP BY IDG;'
    cur.execute(sql)
    raw = cur.fetchone()
    i = 1
    while raw:
        print(f'\nGuide {i} :')
        print(f'    Nom : {raw[1]}')
        print(f'    Prenom : {raw[2]}')
        print(f'    Adresse : {raw[3]}')
        print(f"    Date d'embauche : {raw[4]}")
        print(f'    Nombre de créneaux attribués : {raw[5]}\n')
        raw = cur.fetchone()


def nb_expos():
    cur = conn.cursor()
    sql = 'SELECT num, capacite, COUNT(salle) AS c, SUM(fin - debut) FROM Salle JOIN Exposer ON Salle.num = Exposer.salle JOIN Expo_temp ON Exposer.expo = Expo_temp.nom GROUP BY num ORDER BY c DESC;'
    cur.execute(sql)
    raw = cur.fetchone()
    while raw:
        print(f'\nSalle {raw[0]} :')
        print(f'    Capacité : {raw[1]}')
        print(f"    Nombre d'expositions : {raw[2]}")
        print(f'    Durée cumulée : {raw[3]} jours\n')
        raw = cur.fetchone()


def insert_expotemp():
    cur = conn.cursor()
    cur1 = conn.cursor()
    nom = input("\nEntrez le nom de l'exposition temporaire à ajouter\n")
    date = insert_date()
    try:
        sql = f"INSERT INTO EXPO_TEMP VALUES ('{nom}', TO_DATE({date[0]}, 'YYYY-MM-DD'), TO_DATE({date[1]}, 'YYYY-MM-DD'));"
        cur.execute(sql)
        conn.commit()
    except psycopg2.IntegrityError as e:
        conn.rollback()
        print("\nContrainte non respectée")
        print("Message système :", e)
        return
    sql = "SELECT IDO, titre from Oeuvre_ext WHERE expo IS NULL"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print('\nParmis les oeuvres extérieures suivantes qui ne sont pas dans une exposition temporaire:')
    else:
        print('\nAucune oeuvre extérieure est disponible pour cette exposition')
    while raw:
        print(f"\nIdentifiant de l'oeuvre : {raw[0]}")
        print(f'    Titre : {raw[1]}\n')
        response = int(input(
            "Voulez vous ajouter cette oeuvre à l'exposition (Si oui tapez 1, sinon tapez 0)\n"))
        if response == 1:
            try:
                sql1 = f"UPDATE OEUVRE_EXT SET expo = '{nom}' WHERE IDO = '{raw[0]}'"
                cur1.execute(sql1)
                conn.commit()
            except psycopg2.IntegrityError as e:
                conn.rollback()
                print("\nContrainte non respectée")
                print("Message système :", e)
        raw = cur.fetchone()


def insert_guide():
    cur = conn.cursor()
    nom = input("\nEntrez le nom du guide à ajouter\n")
    prenom = input("\nEntrez le prénom du guide à ajouter\n")
    adresse = input("\nEntrez l'adresse du guide à ajouter\n")
    an = int(input("\nEntrez l'année d'embauche du guide à ajouter\n"))
    mois = int(input("\nEntrez le mois d'embauche du guide à ajouter\n"))
    jour = int(input("\nEntrez le jour d'embauche du guide à ajouter\n"))
    try:
        sql = f"INSERT INTO Guide (nom, prenom, adresse, date_embauche) VALUES ('{nom}', '{prenom}', '{adresse}', TO_DATE('{an}-{mois}-{jour}', 'YYYY-MM-DD')) RETURNING IDG;"
        cur.execute(sql)
        res = cur.fetchone()[0]
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\nContrainte non respectée")
        print("Message système :", e)
        return
    sql = "SELECT * FROM Expo_perm"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        perm = int(input(
            '\nVoulez-vous ajouter à ce guide un créneau sur une exposition permanente ? (Tapez 1 si oui, sinon 0)\n'))
        if perm == 1:
            print('\nLes expositions permanentes sont :')
            while raw:
                print(f"  {raw[0]}")
                raw = cur.fetchone()
            continu = 1
            while continu == 1:
                nom = input("\nQuel est le nom de l'exposition permanente ?\n")
                jour = input("\nSur quel jour le créneau sera attribué ?\n")
                creneau = input(
                    "\nQuel est le créneau à ajouter ? (9-11, 11-13, 13-15, 15-17)\n")
                try:
                    sql = f"INSERT INTO Affecter_Perm VALUES('{nom}', {res}, '{jour}', '{creneau}')"
                    cur.execute(sql)
                    conn.commit()
                except psycopg2.DataError as e:
                    conn.rollback()
                    print("\nContrainte non respectée")
                    print("Message système :", e)
                except psycopg2.IntegrityError as e:
                    conn.rollback()
                    print("\nContrainte non respectée")
                    print("Message système :", e)
                continu = int(
                    input('\nVoulez-vous ajouter un autre créneau ? (Tapez 1 si oui, 0 sinon)\n'))
    sql = "SELECT nom FROM Expo_temp"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        temp = int(input(
            '\nVoulez-vous ajouter à ce guide une exposition temporaire ? (Tapez 1 si oui, sinon 0)\n'))
        if temp == 1:
            print('\nLes expositions temporaires sont :')
            while raw:
                print(f"  {raw[0]}")
                raw = cur.fetchone()
            nom = input("\nQuel est le nom de l'exposition temporaire ?\n")
            try:
                sql = f"INSERT INTO Affecter_Temp VALUES ('{nom}', {res});"
                cur.execute(sql)
                conn.commit()
            except psycopg2.DataError as e:
                conn.rollback()
                print("\nContrainte non respectée")
                print("Message système :", e)


def insert_panneau():
    cur = conn.cursor()
    sql = "SELECT num FROM salle"
    cur.execute(sql)
    raw = cur.fetchone()
    if raw:
        print("Parmis les salles suivantes :")
    while raw:
        print(f"   Salle {raw[0]}")
        raw = cur.fetchone()
    num = int(input(
        "Dans laquelle voulez-vous ajouter un panneau ? (Entrez le numéro de la salle)\n"))
    desc = input("\nQuelle est la description du panneau ?\n")
    try:
        sql = f"INSERT INTO Panneau (salle, description) VALUES ({num}, '{desc}');"
        cur.execute(sql)
        conn.commit()
    except psycopg2.DataError as e:
        conn.rollback()
        print("\nContrainte non respectée")
        print("Message système :", e)


def main():
    cont = 1
    while cont == 1:
        menu()
        cont = int(input("Continuer ? 1 si oui, 0 si non\n> "))
    conn.close()
    print(conn)
    return 0


def menu():
    choice1 = int(input(
        "\nVoulez-vous faire une requête (tapez 0) ou une insertion de données ? (tapez 1)\n> "))

    if choice1 == 0:
        print("\nListe des requêtes :")
        print(" 1.   Liste des guides")
        print(" 2.   Liste des informations des expositions permanentes")
        print(
            " 3.   Liste des informations des expositions temporaires en cours et futures")
        print(" 4.   Identifications des guides d'une exposition donnée")
        print(" 5.   Nombre d'oeuvres et prix moyen d'acquisition des oeuvres d'une exposition donnée")
        print(" 6.   Liste des oeuvres appartenant au Louvre")
        print(" 7.   Oeuvres actuellement prêtées par le Louvre")
        print(" 8.   Afficher le temps moyen des prêts avec chaque musée extérieur")
        print(" 9.   Prix d'acquisition, nombre de restaurations, et coût cumulé des restaurations pour chaque oeuvre")
        print(" 10.  Nombre d'oeuvres actuellement absentes")
        print(" 11.  Oeuvres actuellement empruntées par le Louvre")
        print(" 12.  Oeuvres ayant été créées à une période donnée")
        print(" 13.  Nombre de créneaux d'attribution de visite d'expositions permanentes par guide")
        print(" 14.  Nombre d'expositions accueillies par salles\n")

        choice2 = int(input("Entrer Votre choix\n> "))

        if choice2 == 1:
            liste_guide()

        if choice2 == 2:
            liste_expos_permanentes()

        if choice2 == 3:
            liste_expos_temporaires()

        if choice2 == 4:
            afficher_guide_expo()

        if choice2 == 5:
            info_expo()

        if choice2 == 6:
            liste_oeuvres_louvre()

        if choice2 == 7:
            liste_oeuvre_pretees()

        if choice2 == 8:
            temp_moyen_pret()

        if choice2 == 9:
            informations_oeuvres()

        if choice2 == 10:
            liste_oeuvres_absentes()

        if choice2 == 11:
            liste_oeuvre_empruntees()

        if choice2 == 12:
            liste_oeuvre_periode()

        if choice2 == 13:
            nb_creneaux()

        if choice2 == 14:
            nb_expos()

    if choice1 == 1:
        print("\nInserer des données:")
        print(" 1.  Insertion d'un nouveau musée")
        print(" 2.  Insertion oeuvre extérieure")
        print(" 3.  Creation d'une nouvelle exposition permanente")
        print(
            " 4.  Insertion d'une exposition temporaire et d'oeuvres extérieures exposées")
        print(" 5.  Insertion d'une salle d'exposition")
        print(" 6.  Insertion d'un panneau")
        print(" 7.  Insertion d'un guide et affectation à des expositions\n")

        choice2 = int(input("Entrer Votre choix\n> "))

        if choice2 == 1:
            insert_musee()

        if choice2 == 2:
            insert_oeuvreext()

        if choice2 == 3:
            insert_expoperm()

        if choice2 == 4:
            insert_expotemp()

        if choice2 == 5:
            insert_salle()

        if choice2 == 6:
            insert_panneau()

        if choice2 == 7:
            insert_guide()


if __name__ == '__main__':
    main()
