# GROUPE 2 - MERCREDI 13h
## Membres: 
- Julie PICHON
- Barnabé SELLIER
- Martin SCHNEIDER 
- Océane BORDEAU

## Sujet
Gestion du musée du Louvre
Le musée du Louvre a besoin d'un système de gestion de ses œuvres et expositions. Vous êtes en charge de la modélisation et de l'implémentation d'une base de données permettant de répondre au problème, et développerez une interface permettant l'interaction avec ce système.

## Documents
__UML.pdf__ <br/>
__MLD.txt__ <br/>
__NCD.md__ <br/>
&ensp;&ensp; note de clarification du projet <br/>
__init.sql__ <br/>
&ensp;&ensp;fichier d'initialisation de la base de donnée <br/>
> - creation de tables 
> - vues d'héritages
> - set signifiactif de données <br/>


__applications.py__ : <br/>
code soucre de l'application python finalisée permettant les requetes suivantes<br/>
&ensp;&ensp; 1. Requêtes d'insertions : 
> &ensp;&ensp;- Insérer un autre musée <br/>
> &ensp;&ensp;- Insérer une oeuvre <br/>
> &ensp;&ensp;- Insérer une exposition permanente<br/>
> &ensp;&ensp;- Insérer une exposition temporaire (et y exposer des oeuvres liée à un autre musée)<br/>
> &ensp;&ensp;- Insérer une salle d'exposition ( et la liée a une exposition)<br/>
> &ensp;&ensp;- Insérer un panneau explicatif sur une exposition dans une sallle<br/>
> &ensp;&ensp;- Insérer un guide (et affectation à des exposition permanente et temporaires)<BR/>


&ensp;&ensp; 2. Requêtes de Selection<br/>
> &ensp;&ensp;- Lister les guides avec leur identification, nom, prénom, et date d'embauche.<br/>
> &ensp;&ensp;- Lister toutes les informations des expositions permanentes.<br/>
> &ensp;&ensp;- Lister toutes les informations des expositions temporaires en cours et futures, ainsi que les identifications et noms des salles qui leur sont attribuées.<br/>
> &ensp;&ensp;- Afficher les identifications des guides participant à une exposition donnée (gérer cas exposition permanente et exposition temporaire).<br/>
> &ensp;&ensp;- Afficher le nombre d'oeuvres et le prix moyen d'acquisition des oeuvres d'une exposition donnée.<br/>
> &ensp;&ensp;- Lister les oeuvres appartenant au Louvre (identification unique, titre, dates et type), triées dans cet ordre par type, par date, par titre.<br/>
> &ensp;&ensp;- Identifier de manière unique les oeuvres actuellement prêtées (pas les prêts passés ni futurs) par le Louvre, avec la date de fin du prêt, triées par date de fin du prêt.<br/>
> &ensp;&ensp;- Afficher le temps moyen des prêts avec chaque musée extérieur, identifié de manière unique et avec leur nom et adresse (groupé par musée).<br/>
> &ensp;&ensp;- Indiquer pour chaque oeuvre (inclure les oeuvres appartenant à des musées extérieurs) : le prix d'acquisition, le nombre de restaurations qu'elle a subi, et le coût cumulé des restaurations.<br/>
> &ensp;&ensp;- Compter le nombre d'oeuvres actuellement absentes (= en prêt ou en restauration), groupées par exposition permanente identifiée de manière unique.<br/>
> &ensp;&ensp;- Identifier de manière unique les oeuvres actuellement empruntées par le Louvre, avec l'identification unique et le titre de l'exposition temporaire où elles sont exposées, avec la date de fin de l'emprunt, triées par date de fin de l'emprunt.<br/>
> &ensp;&ensp;- Afficher les oeuvres ayant été créées à une période donnée, avec toutes leurs informations ainsi que l'identification unique, nom, si elles appartiennent ou non au Louvre, les durées éventuelles des expositions dans lesquelles elles sont/ont été/seront exposées.<br/>
> &ensp;&ensp;- Compter le nombre de créneaux d'attribution de visite d'expositions permanentes par guide (avec toutes les informations des guides, pas de distinction par exposition).<br/>
> &ensp;&ensp;- Compter le nombre d'expositions accueillies par salles, par ordre décroissant, avec la durée cumulée, ainsi que l'affichage de toutes les informations de la salle.<br/>


__expo_temp.json__ <br/>
__expo_perm.json__ <br/>
&ensp;&ensp;deux fichiers json offrant une modelisation alternative non relationnelle <br/>
__Nosql.md__ <br/>
&ensp;&ensp;fichier d'explication et d'argumentation sur les solutions NoSQL et R-JSON <br/>



