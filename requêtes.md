__Requêtes de selection__

1)
Lister les guides avec leur identification, nom, prénom, et date d'embauche.
```
SELECT idg, nom, prenom, date_embauche FROM Guide ;
```

2)
Lister toutes les informations des expositions permanentes.
```
SELECT * FROM Expo_perm ;
```

3)
Lister toutes les informations des expositions temporaires en cours et futures, ainsi que les identifications et noms des salles qui leur sont attribuées.
```
SELECT nom, debut, fin, salle FROM Expo_temp ET LEFT OUTER JOIN Exposer E ON ET.nom = E.expo WHERE fin > CURRENT_DATE;
```

4)
Afficher les identifications des guides participant à une exposition donnée (gérer cas exposition permanente et exposition temporaire).
```
SELECT nom, prenom, idg , jour, creneau FROM Guide Gu
INNER JOIN Affecter_perm Ap
ON Gu.idg=Ap.guide 
WHERE Ap.expoperm='Femmes' ;

SELECT nom, prenom, idg FROM Guide Gu
INNER JOIN Affecter_temp At
ON Gu.idg=At.guide 
WHERE At.expotemp='Impressionniste';
```


5)
Afficher le nombre d'oeuvres et le prix moyen d'acquisition des oeuvres d'une exposition donnée.
```
SELECT expo, Count(*) as nombre_oeuvre , Avg(prix_acq) as prix_moyen 
FROM OEUVRE_LOUVRE
GROUP BY expo
HAVING expo='Femmes';
```


6)
Lister les oeuvres appartenant au Louvre (identification unique, titre, dates et type), triées dans cet ordre par type, par date, par titre.
```
SELECT ido, titre, date_crea, type FROM Oeuvre_louvre
ORDER BY type, date_crea ASC, titre ASC;
```


7)
Identifier de manière unique les oeuvres actuellement prêtées (pas les prêts passés ni futurs) par le Louvre, avec la date de fin du prêt, triées par date de fin du prêt.
```
SELECT prete.IDO, titre FROM Prete
JOIN Oeuvre_louvre ON Prete.IDO = Oeuvre_louvre.ido
WHERE debut < CURRENT_DATE AND fin > CURRENT_DATE;
```


8)
Afficher le temps moyen des prêts avec chaque musée extérieur, identifié de manière unique et avec leur nom et adresse (groupé par musée).
```
SELECT musee, adresse, AVG(ROUND(fin - debut)) FROM Prete
JOIN Musee_ext ON prete.musee = musee_ext.IDM
GROUP BY musee, musee_ext.adresse;
```

9)
Indiquer pour chaque oeuvre (inclure les oeuvres appartenant à des musées extérieurs) : le prix d'acquisition, le nombre de restaurations qu'elle a subi, et le coût cumulé des restaurations.
```
SELECT ol.ido as "id", ol.titre as "Oeuvres du Louvre", ol.prix_acq, count(r.oeuvre) as "Nombre restaurations", sum(r.montant) as "Total montant restaurations", ox.ido as "id", ox.titre as "Oeuvres extérieures"
FROM Oeuvre_louvre ol
LEFT JOIN Restaurer r ON r.oeuvre = ol.ido
RIGHT JOIN Oeuvre_ext ox ON ox.ido = ol.ido
GROUP BY ol.ido, ol.titre, ol.prix_acq, ox.ido,ox.titre
ORDER BY ol.ido, ox.ido;
``` 
(moyen d'améliorer)
(problème : si id oeuvre_ext différents de id_oeuvre_louvre)
```
SELECT IDO, titre, COUNT(oeuvre), SUM(montant) FROM Oeuvre_louvre
LEFT OUTER JOIN Restaurer ON Oeuvre_louvre.IDO = Restaurer.oeuvre
GROUP BY IDO;

SELECT IDO, titre FROM Oeuvre_ext;
```
(Version en 2 requêtes)

10)
Compter le nombre d'oeuvres actuellement absentes (= en prêt ou en restauration), groupées par exposition permanente identifiée de manière unique.
```
SELECT nom, COUNT(oeuvre) FROM Expo_perm
JOIN Oeuvre_louvre ON Expo_perm.nom = Oeuvre_louvre.expo
JOIN Restaurer ON Oeuvre_louvre.IDO = Restaurer.oeuvre
JOIN Prete ON Oeuvre_louvre.IDO = Prete.IDO
WHERE (Restaurer.fin > CURRENT_DATE) OR (Prete.debut < CURRENT_DATE AND Prete.fin > CURRENT_DATE)
GROUP BY nom;
```
<> A vérifier avec plus d'inserts

11)
Identifier de manière unique les oeuvres actuellement empruntées par le Louvre, avec l'identification unique et le titre de l'exposition temporaire où elles sont exposées, avec la date de fin de l'emprunt, triées par date de fin de l'emprunt.
```
SELECT Oeuvre_ext.IDO, expo, fin FROM Emprunte
JOIN Oeuvre_ext ON Emprunte.IDO = Oeuvre_ext.IDO
WHERE Emprunte.debut < CURRENT_DATE AND Emprunte.fin > CURRENT_DATE
ORDER BY fin;
```

12)
Afficher les oeuvres ayant été créées à une période donnée, avec toutes leurs informations ainsi que l'identification unique, nom, si elles appartiennent ou non au Louvre, les durées éventuelles des expositions dans lesquelles elles sont/ont été/seront exposées.

```
SELECT * FROM Oeuvre_louvre
WHERE date_crea < 1800 and date_crea > 1700;

SELECT IDO, titre, dimensions, type, date_crea, auteur, expo, ROUND(fin - debut) FROM Oeuvre_ext
JOIN Expo_temp ON Oeuvre_ext.expo = Expo_temp.nom;
WHERE date_crea < 1800 and date_crea > 1700;
```
(Version en 2 requêtes)

13)
Compter le nombre de créneaux d'attribution de visite d'expositions permanentes par guide (avec toutes les informations des guides, pas de distinction par exposition).
```
SELECT IDG, nom, prenom, adresse, date_embauche, COUNT(Guide.IDG) FROM Guide
JOIN Affecter_perm ON Guide.IDG = Affecter_perm.guide
GROUP BY IDG;
```

14)
Compter le nombre d'expositions accueillies par salles, par ordre décroissant, avec la durée cumulée, ainsi que l'affichage de toutes les informations de la salle.
```
SELECT num, capacite, c AS COUNT(salle), SUM(fin - debut) FROM Salle
JOIN Exposer ON Salle.num = Exposer.salle
JOIN Expo_temp ON Exposer.expo = Expo_temp.nom
GROUP BY num ORDER BY c DESC;
```

__Requêtes d'insertion__

1)
Insérer un autre musée.
```
INSERT INTO MUSEE_EXT (nom, adresse, cp, ville) VALUES ('Musée Fenaille', '14 place Raynaldy', '12000', 'Rodez');
```

2)
Insérer une oeuvre du néolithique (sculpture estimée à environ -4000 avant JC, dont l'auteur est inconnu), appartenant au musée créé en 1).
```
INSERT INTO OEUVRE_EXT (titre, type, date_crea) VALUES ('Statue Menhir', 'sculpture', -4000);
INSERT INTO PRETE VALUES (4, 5, TO_DATE('2010-12-05', 'YYYY-MM-DD'), TO_DATE('2025-12-01', 'YYYY-MM-DD'));
```

3)
Insérer une exposition permanente.
```
INSERT INTO EXPO_PERM VALUES ('Les choses');
```

4)
Insérer une exposition temporaire, devant contenir l'oeuvre insérée en 2) (et qui concerne donc le musée créé en 1) ).
```
INSERT INTO EXPO_TEMP VALUES ('Neolithique', TO_DATE('2010-12-05', 'YYYY-MM-DD'), TO_DATE('2025-12-01', 'YYYY-MM-DD'));
UPDATE OEUVRE_EXT SET expo='Neolithique' WHERE titre='Statue Menhir';
```

5)
Insérer une salle d'exposition liée à l'exposition créée en 4).
```
INSERT INTO Salle VALUES (3, 50);
INSERT INTO Exposer VALUES (3, 'Neolithique');
```

6)
Insérer un panneau explicatif lié à l'exposition créée en 4) et/ou à la salle créée en 5).
```
INSERT INTO Panneau (salle, description) VALUES (3, 'Sculpture réalisée sur pierre - artiste inconnu');
```

7)
Insérer un guide, ayant 2 créneaux de travail pour l'exposition permanente créée en 3) et également une affectation à l'exposition temporaire créée en 4).
```
INSERT INTO Guide (nom, prenom, adresse, date_embauche) VALUES ('Mercier', 'Jean', '5 rue du Port à Bateaux 60200 Compiegne', TO_DATE('2010-01-01', 'YYYY-MM-DD'));
INSERT INTO Affecter_Perm VALUES ('Les choses', 3, 'Jeudi', '9-11');
INSERT INTO Affecter_Perm VALUES ('Les choses', 3, 'Jeudi', '11-13');
INSERT INTO Affecter_Temp VALUES ('Neolithique', 3);
```
